#!/usr/bin/env python3

import requests
from PIL import Image
from io import BytesIO


GIF_RESOURCE_API = (
    "http://api.giphy.com/v1/gifs/search?q=",
    "&api_key=DLCVuTK6KZExOS7JoMq82bi5MaI6EbWO&limit=1")


def get_gif(search_term):
    gif = None

    if not search_term:
        return KeyError("No search term provided.")

    gif_results = requests.get(
        GIF_RESOURCE_API[0] +
        search_term +
        GIF_RESOURCE_API[1])

    first_result = gif_results.json().get("data", None)

    if len(first_result):
        gif_first_url = first_result[0].get(
            "images").get("original").get("url")
        print(gif_first_url)
        if gif_first_url:
            gif = requests.get(gif_first_url)

    return gif.content


def display_gif(gif):
    img = Image.open(BytesIO(gif))
    img.show()


def main():

    search_term = input("Search Term:")
    gif = get_gif(search_term)
    display_gif(gif)


if __name__ == '__main__':
    main()
