#!/usr/bin/env python3

import requests
from PIL import Image
from io import BytesIO
from prompt_toolkit import prompt
from prompt_toolkit.completion import WordCompleter

COMPLETION_LIST = [
    'about',
    'above',
    'across',
    'app',
    'apple',
    'appreciate',
    'bad',
    'ball',
    'balloon',
    'bell',
    'cat']

GIF_RESOURCE_API = (
    "http://api.giphy.com/v1/gifs/search?q=",
    "&api_key=DLCVuTK6KZExOS7JoMq82bi5MaI6EbWO&limit=1")


def get_gif(search_term):
    gif = None

    if not search_term:
        return KeyError("No search term provided.")

    try:
        gif_results = requests.get(
            GIF_RESOURCE_API[0] +
            search_term +
            GIF_RESOURCE_API[1])

        first_result = gif_results.json().get("data", None)

        if first_result:
            gif_first_url = first_result[0].get(
                "images").get("original").get("url")

            if gif_first_url:
                gif = requests.get(gif_first_url)

            return gif.content
        else:
            print("No gifs found.")

    except RuntimeError:
        print("Error while accessing online resource.")

    return None


def display_gif(gif):
    if gif:
        img = Image.open(BytesIO(gif))
        img.show()
    else:
        print("No gif to show.")


def main():
    completer = WordCompleter(COMPLETION_LIST)
    search_term = prompt("Search Term:", completer=completer)

    gif = get_gif(search_term)
    display_gif(gif)


if __name__ == '__main__':
    main()
